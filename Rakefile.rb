ENV['HTML_DIR'] ||= './reports'
ENV['environment'] ||= 'local'

desc 'Create HTML report folders'
task :create_dirs do
  FileUtils.mkdir_p(ENV['HTML_DIR']) unless Dir.glob('**/*/').include?('reports')
  FileUtils.mkdir_p('screenshots/') unless Dir.glob('**/*/').include?('screenshots/')
end

desc 'Delete old html reports and failed screenshots locally'
task :clean_report_dir do
  FileUtils.rm_r Dir.glob("#{ENV['HTML_DIR']}/*.html")
  FileUtils.rm_r Dir.glob("#{File.dirname(__FILE__)}/screenshots/*.png")
  FileUtils.rm_r Dir.glob("#{File.dirname(__FILE__)}/screenshots/*.html")
end

# Cleanup for each new test run
Rake::Task[:clean_report_dir].execute
Rake::Task[:create_dirs].execute

namespace :features do
  # Iterate through the cucumber profiles and create a rake task for each profile
  # profiles = YAML::load(File.open(File.dirname(__FILE__) + '/cucumber.yml'))

  # # if running headless install gems and start xvfb
  # if ENV['environment'] == 'jenkins'
  #   system('Xvfb +extension RANDR :99 -ac &')
  # end

  require 'cucumber/rake/task'

  Cucumber::Rake::Task.new(:all, "Runs tests using the 'default' cucumber profile") do |t|
    t.profile = 'default'
    t.cucumber_opts = "--format html -o #{ENV['HTML_DIR']}/results.html -f pretty"
    t.libs << 'lib'
  end

end

