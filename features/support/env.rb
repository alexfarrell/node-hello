require 'rspec'
require 'rspec/expectations'
require 'capybara/dsl'
require 'capybara/cucumber'
require 'capybara/rspec'
require 'selenium-webdriver'
require 'pry'
require 'site_prism'
require 'require_all'
require 'yaml'
require 'rake'

Capybara.register_driver :selenium do |app|
  profile = Selenium::WebDriver::Firefox::Profile.new
  Capybara::Selenium::Driver.new(app, :profile => profile)
end

Capybara.default_driver = :selenium
Capybara.default_max_wait_time = 5
Capybara.app_host = ENV['APP_URL'] || 'http://node-hello.dev.local:1080'

