class Home < SitePrism::Page

  set_url '/'

  element :title, 'h1'
  element :welcome, '#title'
  element :hello_world, 'blockquote'


end