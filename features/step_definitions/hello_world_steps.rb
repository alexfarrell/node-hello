Given(/^I am on hello world home page$/) do
  @app.home.load
end

Then(/^I will be welcomed to success$/) do
  expect(@app.home.title.text).to eq('Success')
  expect(@app.home.welcome.text).to eq('Welcome to Success')
  expect(@app.home.hello_world.text).to eq('Hello From Java')
end
