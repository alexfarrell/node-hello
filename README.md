# Node Hello World

...

# Database setup

1. Manually create some mysql databases. See the `config/config.json` file for details
2. Ensure you have node and npm installed
3. `npm install`
4. `node_modules/.bin/sequelize db:migrate`
5. `node_modules/.bin/sequelize db:seed:all`
6. ???
7. Profit!

# Test Automation

## Pre Requisites

Ruby version 2.2

## Executing Tests

### Locally

```ruby
gem install bundler
bundle install
bundle exec rake features:all
```

### Jenkins

When executing tests through Jenkins we will use a Docker image with Ruby, Firefox, Xvfb and bundler pre installed.

Jenkins shell to execute the tests as follows:

```ruby
bundle install
bundle exec rake features:all environment='jenkins'
```
