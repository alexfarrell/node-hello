var models = require('../models');
var express = require('express');
var http = require('http');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('bpf', { title: 'My account' });
});
router.post('/', function(req, res, next) {
  models.Customer.findAll({
  }).then(function(users) {
    res.render('bpf', {
      title: 'My account',
      details: {
        name: users[0].first_name + " " + users[0].last_name,
        balance: "£" + users[0].balance,
        nextPaymentAmount: "£123.00",
        nextPaymentDate: "1st January 2000"
      }
    });
  });
});

module.exports = router;
