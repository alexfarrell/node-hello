var express = require('express');
var router = express.Router();

/* GET debug page. */
router.get('/', function(req, res, next) {
  res.render('debug', {
    title: "debug",
    env: process.env
  });
});

module.exports = router;
