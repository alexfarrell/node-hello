var express = require('express');
var http = require('http');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {

  var service_name_env = (process.env.SERVICE_NAME+"_BACKEND_SERVICE_HOST").toUpperCase();
  service_name_env = service_name_env.replace(/[^A-Z0-9_]/g,'_');
  var service_name = process.env[service_name_env] || 'java-hello';
  http.get("http://"+service_name+":8080", function(resp) {
    var d = "";
    resp.on('data', function(chunk) {
      d += chunk;
    });
    resp.on('end', function() {
      res.render('index', { title: 'Success', payload: d });
    });
  }).on('error', function(e) {
    res.render('index', { title: 'Failure', payload: e });
  });
});

module.exports = router;
