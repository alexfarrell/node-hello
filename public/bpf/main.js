// Navbar Slide Menu
$(document).ready(function() {   
    var sideslider = $('[data-toggle=collapse-side]');
    var sel = sideslider.attr('data-target');
    var sel2 = sideslider.attr('data-target-2');
    sideslider.click(function(event){
        $(sel).toggleClass('in');
        $(sel2).toggleClass('out');
    });
});

//Navbar Slide Menu Inner
( function( $ ) {
$( document ).ready(function() {
  $('.has-sub').addClass('closed');
  $('#cssmenu li.has-sub>a').on('click', function(){

    $(this).removeAttr('href');
    var element = $(this).parent('li');
    if (element.hasClass('open')) {
      element.removeClass('open');
      element.find('li').removeClass('open');
      element.find('li').addClass('closed');
      element.find('ul').slideUp();
      element.addClass('closed');
      element.siblings('li').removeClass('closed');
      element.siblings('li').find('li').removeClass('closed');
    }
    else {
      element.addClass('open');
      element.children('ul').slideDown();
      element.siblings('li').children('ul').slideUp();
      element.siblings('li').removeClass('open');
      element.siblings('li').addClass('closed');
      element.siblings('li').find('li').removeClass('open');
      element.siblings('li').find('li').addClass('closed');
      element.siblings('li').find('ul').slideUp();
      element.removeClass('closed');

    }
  });
  $('#csmenu li.has-sub>a').on('click', function(){

    $(this).removeAttr('href');
    var element = $(this).parent('li');
    if (element.hasClass('open')) {
      element.removeClass('open');
      element.find('li').removeClass('open');
      element.find('li').addClass('closed');
      element.find('ul').slideUp();
      element.addClass('closed');
      element.siblings('li').removeClass('closed');
      element.siblings('li').find('li').removeClass('closed');
    }
    else {
      element.addClass('open');
      element.children('ul').slideDown();
      element.siblings('li').children('ul').slideUp();
      element.siblings('li').removeClass('open');
      element.siblings('li').addClass('closed');
      element.siblings('li').find('li').removeClass('open');
      element.siblings('li').find('li').addClass('closed');
      element.siblings('li').find('ul').slideUp();
      element.removeClass('closed');

    }
  });
  $('#cssmenu>ul>li.has-sub>a').append('<span class="holder"></span>');
  $('#csmenu>ul>li.has-sub>a').append('<span class="holder"></span>');

  function rgbToHsl(r, g, b) {
      r /= 255, g /= 255, b /= 255;
      var max = Math.max(r, g, b), min = Math.min(r, g, b);
      var h, s, l = (max + min) / 2;

      if(max == min){
          h = s = 0;
      }
      else {
          var d = max - min;
          s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
          switch(max){
              case r: h = (g - b) / d + (g < b ? 6 : 0); break;
              case g: h = (b - r) / d + 2; break;
              case b: h = (r - g) / d + 4; break;
          }
          h /= 6;
      }
      return l;
  }
});
} )( jQuery );

//Cookies
  var countryCookie = "";
    var nameEQ = "countryCode=";
    var currentPagePath = '/content/barclayspublic/en';
    var redirectSel = '';
    var localizedPageToCheck = 'false';
    var wcmModeNotEdit = 'true';
    var contextPath='';

$(document).ready(function(){ 
 var countryCookie = $.cookie("countryCode");
 if(countryCookie!=null ){
    if(countryCookie.length != 0){
         redirect(countryCookie,nameEQ,currentPagePath,redirectSel,localizedPageToCheck,wcmModeNotEdit,contextPath);
    }
  } 
});

//Acessibility + ARIA
$(document).ready(function(){
  $('.breadcrumb li:first-child a').attr('title', 'Home page link').append('<span class="visuallyhidden">Home page link</span>');
  //hamburger button
  $('.logo-banner__hamburger button').attr({
    role: 'button',
    'aria-controls': 'navigation',
    'aria-expanded': 'false',
    title: 'Open menu'
  }).append('<span class="visuallyhidden">Open menu</span>');
  $('.logo-banner__hamburger button').click(function(){
    if ($('div.side-collapse-container').hasClass('out')) {
      $('.logo-banner__hamburger button .visuallyhidden').remove();
      $('.logo-banner__hamburger button').attr({'aria-expanded': 'true', title: 'Close menu'}).append('<span class="visuallyhidden">Close menu</span>');
    } else {
      $('.logo-banner__hamburger button .visuallyhidden').remove();
      $('.logo-banner__hamburger button').attr({'aria-expanded': 'false', title: 'Open menu'}).append('<span class="visuallyhidden">Open menu</span>');
    }
  });
  //Side nav
  $('#cssmenu li.has-sub > a').attr({'aria-expanded': 'false', role: 'tab'});
  $('#cssmenu li.open').parents('a').attr('aria-expanded', 'true');
  $('#cssmenu li.closed').parents('a').attr('aria-expanded', 'false');
  $('#cssmenu li.has-sub > a').click(function(){
    $('li.open > a').attr('aria-expanded', 'true');
    $('li.closed > a').attr('aria-expanded', 'false');
  });
  $(".logo-banner__hamburger button").click(function() {
    $("#cssmenu ul li:first-child > a").focus();
  });
  $(document).keypress(function(e) {
    if(e.which == 13) {
        alert('You pressed enter!');
    }
  });
  $("#cssmenu li.has-sub > a").click(function() {
    $(this).children('a').focus();
  });
  //Side nav cs
  $('#csmenu li.has-sub > a').attr({'aria-expanded': 'false', role: 'tab'});
  $('#csmenu li.open').parents('a').attr('aria-expanded', 'true');
  $('#csmenu li.closed').parents('a').attr('aria-expanded', 'false');
  $('#csmenu li.has-sub > a').click(function(){
    $('li.open > a').attr('aria-expanded', 'true');
    $('li.closed > a').attr('aria-expanded', 'false');
  });
  $(".logo-banner__hamburger button").click(function() {
    $("#csmenu ul li:first-child > a").focus();
  });
  $(document).keypress(function(e) {
    if(e.which == 13) {
        alert('You pressed enter!');
    }
  });
  $("#csmenu li.has-sub > a").click(function() {
    $(this).children('a').focus();
  });
  //Logo alt text fix
  $('.logo img').attr( "alt", "Barclaycard logo" );
});


